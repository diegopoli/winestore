package com.ctl.stk.product.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Product entity
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Entity
public class Product {
	
	/**
	 * Product identification
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Product sku
	 */
	private String sku;
	
	/**
	 * Product name
	 */
	private String name;
		
	/**
	 * Price of product
	 */	
	@Column(precision=8, scale=2)
	private BigDecimal price;	  
	
	/**
	 * Product image
	 */
	private String image;
	
	/**
	 * Get identification
	 * @return identification of product
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Set product identification
	 * @param identification of id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Get sku
	 * @return product sku
	 */
	public String getSku() {
		return sku;
	}

	/**
	 * Set sku
	 * @param product sku
	 */
	public void setSku(String sku) {
		this.sku = sku;
	}

	/**
	 * Get product name
	 * @return product name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set product name
	 * @param product name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Get price
	 * @return
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * Set price
	 * @param price
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * Get image
	 * @return product image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Set image
	 * @param product image
	 */
	public void setImage(String image) {
		this.image = image;
	}

}

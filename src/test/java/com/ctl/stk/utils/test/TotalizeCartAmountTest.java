package com.ctl.stk.utils.test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.ctl.stk.shoppingcart.entity.ShoppingCart;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;
import com.ctl.stk.shoppingcart.utils.TotalizeShoppingCartAmount;

/**
 * Totalize cart amount test
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class TotalizeCartAmountTest {
	
	@Before
	public void init() {}
	
	/**
	 * Test as json string method
	 * @throws Exception
	 */
	@Test
	public void testCalculate() throws Exception {
		
		//create first shopping cart item
		ShoppingCartItem itemOne = new ShoppingCartItem();
		itemOne.setAmount(new BigDecimal(90.1));
		
		//create other shopping cart item
		ShoppingCartItem itemTwo = new ShoppingCartItem();
		itemTwo.setAmount(new BigDecimal(1.2));
		
		//create shopping cart
		ShoppingCart cart = new ShoppingCart();
		cart.addItem(itemOne);
		cart.addItem(itemTwo);
		
		//generate results
		String result = (new TotalizeShoppingCartAmount()).calculate(cart).toPlainString();
		String expected =  "91.2999999999999942712491929341922514140605926513671875";
		
		//test
		assertEquals(expected, result);
	}
}

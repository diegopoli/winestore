/**
 * Application services comunicator
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
var WscServices = (function(){
 
	/**
	 * Application content type
	 */
	const CONTENT_TYPE = 'application/json';
		
	/**
	 * Json service type
	 */
	const DATA_TYPE_JSON = 'json';
	
	/**
	 * Post invoke method
	 */
	const METHOD_POST = 'POST';
	
	/**
	 * Get invoke method
	 */
	const METHOD_GET = 'GET';
	
	/**
	 * Put invoke method
	 */
	const METHOD_PUT = 'PUT';
	
	/**
	 * Delete invoke method
	 */
	const METHOD_DELETE = 'DELETE';
	
	/**
	 * Request headers
	 */
	headers = function(){
		return { 'Content-Type': CONTENT_TYPE }
	};

	/**
	 * Invoke service
	 * @param action of service
	 * @param data
	 * @para type of method
	 */
	invoke = function(action, data, type, dataType){
		return $.ajax({
			headers: headers(),
			type: type,
			url: action,
			async: false,
  		  	dataType: dataType,
  		  	data: JSON.stringify(data),
  		  	success: function(data){ 
  		  		return data;
  		  	}
		});
	};
	
	/**
	 * Execute post service
	 * @param action of service
	 * @param data
	 * @returns result service data
	 */
	executePost = function(action, data){
		return invoke(action, data, METHOD_POST, DATA_TYPE_JSON);
	};
	
	/**
	 * Execute post data
	 * @param action of service
	 * @param data
	 * @returns result service data
	 */
	executePut = function(action, data){
		return invoke(action, data, METHOD_PUT, '');
	};
	
	/**
	 * Execute delete service
	 * @param action of service
	 * @param data of service
	 */
	executeDelete = function(action, data){
		return invoke(action, data, METHOD_DELETE, '');
	};
	
	/**
	 * Execute get service
	 * @param action of service
	 */
	executeGet = function(action){
         return invoke(action, '', METHOD_GET, DATA_TYPE_JSON);
	};
  
	return { 
		
		/**
		 * Public execute post method
		 */
		executePost: function(action, data) {
			return executePost(action, data);
		},
		
		/**
		 * Public execute delete method
		 */
		executeDelete: function(action, data) {
          return executeDelete(action, data);
        },
        
        /**
         * Public execute get method
         */
        executeGet: function(action) {
        	return executeGet(action);
        },
        
        /**
         * Public execute put method
         */
        executePut: function(action, data) {
			return executePut(action, data);
		}
  };
})();

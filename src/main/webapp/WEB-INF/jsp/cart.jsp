<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:template>
    <jsp:body>
    	<div class="container wsc-cart-container" data-bind="visible: wscController.items().length > 0">
			<div> 
	    		<div class="shopping-cart">
	        		<div class="column-labels">
	    	    		<label class="product-image">Image</label>
	             		<label class="product-details">Product</label>
	    				<label class="product-price">Price</label>
	    				<label class="product-quantity">Quantity</label>
	    				<label class="product-removal">Remove</label>
	   					<label class="product-line-price">Total</label>
	  				</div>
	  				<div data-bind="foreach: wscController.items">
		   				<div class="product">
		    				<div class="product-image">
		      					<img data-bind="attr:{src: product.image}" class="col-sm-9">
		    				</div>
		    				<div class="product-details">
			      				<div class="product-title">
			      					<h5 data-bind="text: product.name"></h5>
		      					</div>
		    				</div>
		    				<div class="product-price">
			    				<div class="w-product-price-label price-label" data-bind="money: product.price"></div>    
		    				</div>
		    				<div class="product-quantity">
		      					<input type="number" value="2" min="1" data-bind="value: quantity, event: { change: wscController.changeQuantityCart }">
		    				</div>
		    				<div class="product-removal">
		      					<button class="btn w-btn-remove-product" data-bind="click: wscController.removeProductCart">
		        					<i class="material-icons">clear</i>
									Remove
		      					</button>
		    				</div>
		    				<div class="product-line-price">
		    					<div class="w-product-price-label price-label" data-bind="money: amount"></div>
		    				</div>
		  				</div>
	 				</div>
	  				<div class="totals">
	    				<div class="totals-item">
	      					<label>Subtotal</label>
	      					<div class="totals-value" id="cart-subtotal">
	      						<div class="w-product-price-label price-label" data-bind="money: wscController.amount"></div>
	      					</div>
	   					</div>
	    				<div class="totals-item">
	      					<label>Shipping (free)</label>
	      					<div class="totals-value" id="cart-shipping">0.00</div>
	    				</div>
	    				<div class="totals-item totals-item-total">
	      					<label>Grand Total</label>
	      					<div class="totals-value" id="cart-total">
	      						<div class="w-product-price-label price-label" data-bind="money: wscController.amount"></div>
	      					</div>
	    				</div>
	  				</div>	      			
				</div>							
	    	</div>
		</div>
		<div class="container wsc-empty-cart" data-bind="visible: wscController.items().length == 0">
			<div class="row">
				<div class="wsc-cart-message">
					Your shopping cart is empty :(
				</div>
			</div>
		</div>
	</jsp:body>
</t:template>
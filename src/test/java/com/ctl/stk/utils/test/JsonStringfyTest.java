package com.ctl.stk.utils.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.ctl.stk.product.entity.Product;
import com.ctl.stk.utils.JsonStrinfy;

/**
 * Json stringfy test
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class JsonStringfyTest {
	
	@Before
	public void init() {}
	
	/**
	 * Test as json string method
	 * @throws Exception
	 */
	@Test
	public void testAsJsonString() throws Exception {
		
		//spected result
		String spected = "{\"id\":null,\"sku\":\"2S8F0E\",\"name\":\"Wine win two\",\"price\":null,\"image\":null}";
		
		//create object
		Product prod = new Product();
		prod.setName("Wine win two");
		prod.setSku("2S8F0E");
		
		//test
		assertEquals(spected, JsonStrinfy.asJsonString(prod));
	}
}

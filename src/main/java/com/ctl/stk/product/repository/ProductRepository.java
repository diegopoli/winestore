package com.ctl.stk.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ctl.stk.product.entity.Product;

/**
 * Product repository
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Repository("productRepository")
public interface ProductRepository extends JpaRepository<Product, Long>{}

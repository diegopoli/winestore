package com.ctl.stk.shoppingcart.service;

import java.util.List;

import com.ctl.stk.shoppingcart.entity.ShoppingCart;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;

/**
 * Cart service interface
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public interface ShoppingCartService {
	
	/**
	 * Get cart by id
	 * @param id of cart
	 * @return cart object
	 */
	ShoppingCart getCartById(Long id);
	
	/**
	 * Get all cart
	 * @return all carts
	 */
	List<ShoppingCart> getAllCart();
	
	/**
	 * Add cart
	 * @param cart
	 * @return cart
	 */
	ShoppingCart addCart(ShoppingCart cart);
	
	/**
	 * Calculate shopping cart
	 * @param id
	 * @return shopping cart
	 */
	ShoppingCart caculate(Long id);
	
	/**
	 * Create shopping or not exist
	 * @param id
	 * @return shopping cart
	 */
	ShoppingCart createOrNotExist(Long id);
	
	/**
	 * Add to cart item
	 * @param shopping cart item
	 * @param shoppuing cart id
	 * @return shopping cart
	 */
	ShoppingCart addItem(ShoppingCartItem item, Long id);

}

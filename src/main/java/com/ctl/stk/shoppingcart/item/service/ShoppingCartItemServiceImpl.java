package com.ctl.stk.shoppingcart.item.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;
import com.ctl.stk.shoppingcart.item.repository.ShoppingCartItemRepository;

/**
 * Shopping cart item service implementation
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Service("shoppingCartItemService")
public class ShoppingCartItemServiceImpl implements ShoppingCartItemService {

	/**
	 * Cart repository
	 */
	@Autowired
	ShoppingCartItemRepository shoppingCartItemRepository;
	
	/**
	 * Get cart by identification
	 */
	@Override
	public ShoppingCartItem getItemtById(Long id) {
		return shoppingCartItemRepository.findById(id).get();
	}
	
	/**
	 * Add cart
	 */
	@Override
	public ShoppingCartItem addItem(ShoppingCartItem shoppingCartItem) {	
		return shoppingCartItemRepository.save(shoppingCartItem);		
	}
	
	/**
	 * Add item and calculate amount
	 * @param shopping cart item
	 * @return shopping cart item
	 */
	@Override
	public ShoppingCartItem addItemAndCalculateAmount(ShoppingCartItem shoppingCartItem) {
		shoppingCartItem.calculateAmount();
		return addItem(shoppingCartItem);
		
	}

	/**
	 * Get all shopping cart items
	 */
	@Override
	public List<ShoppingCartItem> getAll() {
		return shoppingCartItemRepository.findAll();
	}

	/**
	 * Delete by identification
	 */
	@Override
	public void deleteById(Long id) {
		shoppingCartItemRepository.deleteById(id);
	}
	
	/**
	 * Change quantity item
	 * @param item id
	 * @param item quantity
	 * @return iten changed
	 */
	public ShoppingCartItem changeQuantity(Long id, int quantity) {
		ShoppingCartItem item = getItemtById(id);
		item.setQuantityAndCalculateAmount(quantity);
		return addItem(item);			
	}
}

package com.ctl.stk.product.service;

import java.util.List;

import com.ctl.stk.product.entity.Product;

/**
 * Product service interface
 * @author Diego Andre Poli
 */
public interface ProductService {
	
	/**
	 * Get product by id
	 * @param id as long
	 * @return product
	 */
	Product getProductById(Long id);
	
	/**
	 * Get all products
	 * @return list of products
	 */
	List<Product> getAllProducts();
	
	/**
	 * Add product
	 * @param product object
	 * @return product object
	 */
	Product addProduct(Product product);

}

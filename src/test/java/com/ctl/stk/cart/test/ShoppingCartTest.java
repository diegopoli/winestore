package com.ctl.stk.cart.test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ctl.stk.product.entity.Product;
import com.ctl.stk.product.service.ProductService;
import com.ctl.stk.session.SessionHandler;
import com.ctl.stk.shoppingcart.controller.ShoppingCartController;
import com.ctl.stk.shoppingcart.entity.ShoppingCart;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;
import com.ctl.stk.shoppingcart.item.service.ShoppingCartItemService;
import com.ctl.stk.shoppingcart.service.ShoppingCartService;
import com.ctl.stk.utils.JsonStrinfy;

/**
 * Shopping cart test
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class ShoppingCartTest {

	private MockMvc mvc;

	@Mock
	private ProductService productService;

	@Mock
	private ShoppingCartService cartService;

	@Mock
	private ShoppingCartItemService shoppingCartItemService;

	@InjectMocks
	private ShoppingCartController cartController;

	@Before
	public void init() {
		//init mockito
		MockitoAnnotations.initMocks(this);
		
		//initialize controller
		mvc = MockMvcBuilders.standaloneSetup(cartController).build();
	}

	/**
	 * Test get shopping cart
	 * @throws Exception
	 */
	@Test
	public void testGetCart() throws Exception {

		//create cart
		ShoppingCart cart = new ShoppingCart();
		cart.setId(9L);
		cart.setAmount(new BigDecimal(8.0));
		
		//add cart to service
		doReturn(cart).when(cartService).createOrNotExist(9L);

		//perform test on get
		mvc.perform(get("/shoppingcart")
			.sessionAttr(SessionHandler.CART_ID, 9L)
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().string(JsonStrinfy.asJsonString(cart)));
	}
	
	/**
	 * Test post shopping cart item
	 * @throws Exception
	 */
	@Test
	public void testPostItem() throws Exception {

		//create cart
		ShoppingCart cart = new ShoppingCart();
		cart.setId(9L);
		cart.setAmount(new BigDecimal(9));

		//create product
		Product product = new Product();
		product.setId(1L);
		product.setSku("34534");
		product.setName("Espumante Real De Aragon");
		product.setPrice(new BigDecimal(9));
		
		//create shopping cart item 
		ShoppingCartItem item = new ShoppingCartItem();
		item.setId(1L);
		item.setProduct(product);
		item.setAmount(new BigDecimal(9));
		item.setQuantity(1);
		
		//create list of items
		List<ShoppingCartItem> items = new ArrayList<ShoppingCartItem>();		
		cart.setItems(items);
		
		//check is product exist
		when(productService.getProductById(product.getId())).thenReturn(product);
		when(cartService.addItem(item, cart.getId())).thenReturn(cart);

		//perform test
		mvc.perform(post("/shoppingcart/items").sessionAttr("cartId", cart.getId())
			.contentType(MediaType.APPLICATION_JSON)
			.content(JsonStrinfy.asJsonString(item)))
			.andExpect(status().isOk());
	}
	
	/**
	 * Post quantity cart item
	 * @throws Exception
	 */
	@Test
	public void testPostItemQuantity() throws Exception {

		//create cart
		ShoppingCart cart = new ShoppingCart();
		cart.setId(9L);
		cart.setAmount(new BigDecimal(9));
		
		//create product
		Product product = new Product();
		product.setId(1L);
		product.setSku("34534");
		product.setName("Espumante Real De Aragon");
		product.setPrice(new BigDecimal(9));
		
		//create shopping cart item
		ShoppingCartItem item = new ShoppingCartItem();
		item.setId(1L);
		item.setProduct(product);
		item.setAmount(new BigDecimal(9));
		item.setQuantity(1);
		
		//create list of shopping cart items
		List<ShoppingCartItem> items = new ArrayList<ShoppingCartItem>();		
		cart.setItems(items);
		
		//check is product exist		
		when(cartService.getCartById(cart.getId())).thenReturn(cart);
		when(productService.getProductById(product.getId())).thenReturn(product);
		when(shoppingCartItemService.getItemtById(product.getId())).thenReturn(item);

		//do result
		doReturn(cart).when(cartService).addCart(cart);
		doReturn(cart).when(cartService).createOrNotExist(9L);

		//perform test
		mvc.perform(put("/shoppingcart/items/1/quantity/1")
			.sessionAttr("cartId", cart.getId())
			.contentType(MediaType.APPLICATION_JSON_UTF8))
			.andExpect(status().isOk());				
	}

}

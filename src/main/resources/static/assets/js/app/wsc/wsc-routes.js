
/**
 * Application services routes
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
var WscRoutes = (function(){
	
	/**
	 * All products route
	 */
	const GET_PRODUCT = '/products';
	
	/**
	 * Shooping cart routes
	 */
	const GET_SHOPPING_CART = '/shoppingcart';
	 
	/**
	 * Shopping cart items route
	 */
	const GET_SHOPPING_CART_ITEMS = '/shoppingcart/items';
	
	/**
	 * Shopping cart item
	 */
	const GET_SHOPPING_CART_ITEM = '/shoppingcart/items/{id}';

	/**
	 * Shopping cart item quantity
	 */
	const GET_SHOPPING_CART_ITEM_QTY = '/shoppingcart/items/{id}/quantity/{qty}';
		 
	return { 
		
		/**
		 * Public get all product route
		 */
		getAllProduct: function() {
			return GET_PRODUCT;
		},
	
		/**
		 * Public shopping cart
		 */
		getShoppingCart: function() {
			return GET_SHOPPING_CART;
		},
		
		/**
		 * Public shopping cart items
		 */
		getShoppingCartItems: function() {
			return GET_SHOPPING_CART_ITEMS;
		},
		
		/**
		 * Public shopping cart item
		 */
		getShoppingCartItem: function(id) {
			return GET_SHOPPING_CART_ITEM.replace('{id}', id);	
		},
		
		/**
		 * Public shopping cart item quantity
		 * TODO: Replace property can be replaced by prototype when it is present :P
		 */
		getShoppingCartItemQty: function(id, qty) {
			return GET_SHOPPING_CART_ITEM_QTY.replace('{id}', id).replace('{qty}', qty);	
		}
  };
})();

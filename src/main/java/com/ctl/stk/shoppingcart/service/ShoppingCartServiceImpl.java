package com.ctl.stk.shoppingcart.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctl.stk.shoppingcart.entity.ShoppingCart;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;
import com.ctl.stk.shoppingcart.repository.ShoppingCartRepository;

/**
 * Cart service implementation
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Service("cartService")
public class ShoppingCartServiceImpl implements ShoppingCartService {

	/**
	 * Cart repository
	 */
	@Autowired
	ShoppingCartRepository cartRepository;
	
	/**
	 * Get cart by identification
	 */
	@Override
	public ShoppingCart getCartById(Long id) {
		return cartRepository.findById(id).get();
	}
	
	/**
	 * Is exist shopping cart
	 * @param shopping cart id
	 * @return true is exist shopping cart
	 */
	public boolean existsById(Long id) {
		if(id == null) {
			return false;
		}
		
		return cartRepository.existsById(id);
	}

	/**
	 * Get all carts
	 */
	@Override
	public List<ShoppingCart> getAllCart() {
		return cartRepository.findAll();
	}
	
	/**
	 * Add cart
	 */
	@Override
	public ShoppingCart addCart(ShoppingCart cart) {
		cart.calculateAmount();
		return cartRepository.save(cart);
	}
	
	/**
	 * Calculate shopping cart
	 * @param shopping cart id
	 * @return shopping cart
	 */
	public ShoppingCart caculate(Long id) {
		ShoppingCart cart = getCartById(id);
		cart.calculateAmount();
		return addCart(cart);
	}
	
	/**
	 * Create or not exist shopping cart
	 * @param shopping cart id
	 * @return shopping cart
	 */
	public ShoppingCart createOrNotExist(Long id) {
		if (existsById(id)) {
			return getCartById(id);
		}
		
		return addCart(new ShoppingCart());
	}
	
	/**
	 * Add item to shopping cart
	 * @param shopping cart item
	 * @param shopping cart id
	 * @return shopping cart
	 */
	public ShoppingCart addItem(ShoppingCartItem item, Long id) {
		ShoppingCart cart = getCartById(id);
		cart.mergeItems(item);
		return addCart(cart);
	}
}

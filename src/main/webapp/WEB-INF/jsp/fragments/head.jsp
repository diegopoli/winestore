<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">		
<link rel="stylesheet" href="/assets/css/vendor/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/app/wsc/app.css" >

<script type='text/javascript' src="/assets/js/vendor/jquery/jquery-min.js"></script>
<script type='text/javascript' src='/assets/js/vendor/knockout/knockout.js'></script>
<script type='text/javascript' src='/assets/js/vendor/knockout/knockout.mapping.js'></script>
<script type='text/javascript' src='/assets/js/vendor/notify/notify.js'></script>

<script type='text/javascript' src='/assets/js/app/wsc/wsc-binder.js'></script>
<script type='text/javascript' src='/assets/js/app/wsc/wsc-routes.js'></script>
<script type='text/javascript' src='/assets/js/app/wsc/wsc-services.js'></script>
<script type='text/javascript' src='/assets/js/app/wsc/wsc-model.js'></script>
<script type='text/javascript' src='/assets/js/app/wsc/wsc-notify.js'></script>
<script type='text/javascript' src='/assets/js/app/wsc/wsc-app.js'></script>

<script>
	var currenncy = "$";
</script>

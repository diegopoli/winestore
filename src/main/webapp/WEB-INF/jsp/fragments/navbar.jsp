<nav class="navbar navbar-light bg-light">
	<a class="navbar-brand" href="/">
    	<img src="/assets/img/logo.jpg" /> Winestore
  	</a>
  	<form class="form-inline">
    	<a class="btn btn-outline-success w-nav-btn-cart" href="/cart">
	    	<i class="material-icons">shopping_basket</i>
	    	My Bag
	    </a>
  	</form>
</nav>
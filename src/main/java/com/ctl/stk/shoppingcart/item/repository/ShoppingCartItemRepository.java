package com.ctl.stk.shoppingcart.item.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;

/**
 * Shopping cart item repository
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Repository("shoppingCartItemRepository")
public interface ShoppingCartItemRepository extends JpaRepository<ShoppingCartItem, Long>{}

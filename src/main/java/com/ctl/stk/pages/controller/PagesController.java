package com.ctl.stk.pages.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Pages controller
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Controller
public class PagesController {

	/**
	 * Product view name
	 */
	private static final String HOME_VIEW = "home";
	
	/**
	 * Shopping cart view
	 */
	private static final String SHOPPINGCART_VIEW = "cart";

	/**
	 * Home page application
	 * @param http request
	 * @return name of view
	 */
	@RequestMapping(value="/")
	public String cart(HttpServletRequest request) {
		return HOME_VIEW;
	}
	
	/**
	 * Shopping cart page handler
	 * @param http request
	 * @return name of shopping cart view
	 */
	@RequestMapping(value="/cart")
	public String shoppingcart(HttpServletRequest request) {
		return SHOPPINGCART_VIEW;
	}

}

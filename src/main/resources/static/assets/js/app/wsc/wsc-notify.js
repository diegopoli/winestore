/**
 * Wsc application notification
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
var WscNotify = (function(){
 
	/**
	 * Message type success
	 */
	const TYPE_SUCCESS = 'success';
	
	/**
	 * Success of add cart product
	 * TODO: Can be used with spring property bundles.
	 */
	const MSG_PRODUCT_CART_ADD = 'Product added to cart!';
	
	/**
	 * Messa of remove product cart
	 * TODO: Can be used with spring property bundles.
	 */
	const MSG_PRODUCT_CART_REMOVE = 'Product removed from cart!';
	
	/**
	 * Message of change cart item quantity
	 * TODO: Can be used with spring property bundles.
	 */
	const MSG_PRODUCT_CART_QTY = 'Quantity of product changed!';
	
	/**
	 * Execute notify library
	 */
	message = function(msg, type){
		return $.notify(msg, type);	
	};
		 
	return { 
		
		/**
		 * Public add cart product message
		 */
		addCartProduct: function() {
			return message(MSG_PRODUCT_CART_ADD, TYPE_SUCCESS);
		},
		
		/**
		 * Public remove cart product
		 */
		removeCartProduct: function() {
			return message(MSG_PRODUCT_CART_REMOVE, TYPE_SUCCESS);
		},
		
		/**
		 * Public qty cart pruduct change
		 */
		qtyCartProduct: function() {
			return message(MSG_PRODUCT_CART_QTY, TYPE_SUCCESS);
		}
  };
})();

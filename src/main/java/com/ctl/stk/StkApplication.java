package com.ctl.stk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Main stk application startup
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan
public class StkApplication {

	/**
	 * Main application startup
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(StkApplication.class, args);
	}

}

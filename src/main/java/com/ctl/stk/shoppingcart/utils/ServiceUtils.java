package com.ctl.stk.shoppingcart.utils;

/**
 * Service api utils
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class ServiceUtils {
	
	/**
	 * Service empty return
	 * TODO: change to use apache common lang string utils
	 */
	public static final String EMPTY = "";
	
}

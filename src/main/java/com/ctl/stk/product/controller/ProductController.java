package com.ctl.stk.product.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ctl.stk.product.entity.Product;
import com.ctl.stk.product.service.ProductService;

/**
 * Product contoller
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@RestController
@RequestMapping("/products")
public class ProductController {
	
	/**
	 * Product service
	 */
	@Autowired
	private ProductService productService;
	
	/**
	 * Get products
	 * @param http request
	 * @return product object
	 */
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Product> getProducts(HttpServletRequest request){	
		return productService.getAllProducts();
	}
	
	/**
	 * Get product
	 * @param id of product
	 * @return product object
	 */
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Product getProduct(@PathVariable("id") Long id) {
		return productService.getProductById(id);
	}
	
	/**
	 * Add product
	 * @param product
	 * @return product
	 */
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public Product addProduct(@RequestBody Product product) {		
		return productService.addProduct(product);
	}
	
}

package com.ctl.stk.shoppingcart.item;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.ctl.stk.product.entity.Product;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;

/**
 * Shopping cart item test
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 *
 */
public class ShoppingCartItemTest {

	@Before
	public void init() {}
	
	/**
	 * Test defaultMethods
	 * @throws Exception
	 */
	@Test
	public void testDefaultMethods() throws Exception {
		
		//create product
		Product prod = new Product();
		prod.setName("Wine win two");
		prod.setSku("2S8F0E");
		prod.setPrice(new BigDecimal(40.1));
		
		//create shopping cart item
		ShoppingCartItem item = new ShoppingCartItem();
		item.setProduct(prod);
		item.setQuantity(2);
		item.setId(234234L);
		item.calculateAmount();
		
		//results
		String itemAmountExpected = "80.20000000000000284217094304040074348449707031250";
		String itemAmountResult = item.getAmount().toString();
		
		//test
		assertEquals(itemAmountExpected, itemAmountResult);
		assertEquals(2, item.getQuantity());
		assertEquals(234234, item.getId().intValue());
		assertEquals("2S8F0E", item.getProduct().getSku());
	}
}

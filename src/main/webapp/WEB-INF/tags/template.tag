<%@tag description="Winestore" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="shortcut icon" href="/assets/img/logo.png" type="image/x-icon" />
		
		<title>Winestore</title>
		<jsp:include page="fragments/head.jsp" />
	</head>
  	<body>
  		<jsp:include page="fragments/navbar.jsp" />
    	<div id="body">
      		<jsp:doBody/>
    	</div>
    	<jsp:include page="fragments/footer.jsp" />
  </body>
</html>

/**
 * Money knockout binder
 * Binder number format and currency by system currency
 */
ko.bindingHandlers.money = { 
		
	/**
	 * Update money binder 
	 */
    update: function(element, valueAccessor, allBindingsAccessor) {
    	
    	//initial values
    	var m = '';
    	var c = currenncy.concat(' ');
    	
    	//value observable
    	var value = valueAccessor(), allBindings = allBindingsAccessor(); 
        var valueUnwrapped = ko.utils.unwrapObservable(value); 
        
        //geenerate value
        if (valueUnwrapped) {
        	m = c.concat(valueUnwrapped.toFixed(2));           
        }
        
        //apply value :)
        $(element).text(m);    
    } 
};
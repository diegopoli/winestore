package com.ctl.stk.shoppingcart.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ctl.stk.product.entity.Product;
import com.ctl.stk.product.service.ProductService;
import com.ctl.stk.session.SessionHandler;
import com.ctl.stk.shoppingcart.entity.ShoppingCart;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;
import com.ctl.stk.shoppingcart.item.service.ShoppingCartItemService;
import com.ctl.stk.shoppingcart.service.ShoppingCartService;
import com.ctl.stk.shoppingcart.utils.ServiceUtils;

/**
 * Cart controller
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@RestController
@RequestMapping("/shoppingcart")
public class ShoppingCartController {
	
	/**
	 * Product service
	 */
	@Autowired
	private ProductService productService;
	
	/**
	 * Shopping cart service
	 */
	@Autowired
	private ShoppingCartService cartService;
	
	/**
	 * Shopping cart item service
	 */
	@Autowired
	private ShoppingCartItemService shoppingCartItemService;
	
	/**
	 * Add shopping cart item
	 * @param service request 
	 * @param product id
	 * @param quantity
	 * @return cart
	 */
	@RequestMapping(value="/items", method=RequestMethod.POST)
	public ShoppingCart addShoppingCartItem(HttpServletRequest request, @RequestBody ShoppingCartItem item) {		
		//get session cart id
		Long id = SessionHandler.getCartId(request);
		
		//get product inforamtion
		Product product = productService.getProductById(item.getProduct().getId());
		
		//add item to cart
		return cartService.addItem((new ShoppingCartItem(product, item.getQuantity())), id);
	}
	
	/**
	 * Get product
	 * @param service request
	 * @param item id
	 * @return empty data
	 */
	@RequestMapping(value="/items/{id}", method=RequestMethod.DELETE)
	public String getProduct(HttpServletRequest request, @PathVariable("id") Long id) {		
		//delete item
		shoppingCartItemService.deleteById(id);

		//calculate cart amount
		cartService.caculate(SessionHandler.getCartId(request));		
		return ServiceUtils.EMPTY;
	}
	
	/**
	 * Set product quantity
	 * @param service request
	 * @param shopping cart item id
	 * @param quantity
	 * @return empty data
	 */
	@RequestMapping(value="/items/{id}/quantity/{quantity}", method=RequestMethod.PUT)
	public String setQuantity(HttpServletRequest request, @PathVariable("id") Long id, @PathVariable("quantity") int quantity) {
		//change item quantity
		shoppingCartItemService.changeQuantity(id, quantity);
		
		//calculate amount shopping cart
		cartService.caculate(SessionHandler.getCartId(request));				
		return ServiceUtils.EMPTY;
	}
	
	/**
	 * Add product to cart
	 * @param service request
	 * @return cart
	 */
	@RequestMapping(value="", method=RequestMethod.GET)
	public ShoppingCart addCart(HttpServletRequest request) {						
		//create new shopping cart
		ShoppingCart cart = cartService.createOrNotExist(SessionHandler.getCartId(request));
		
		//save session shopping cart
		SessionHandler.setCartId(request, cart.getId());		
		return cart;
	}
}

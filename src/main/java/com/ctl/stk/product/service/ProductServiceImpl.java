package com.ctl.stk.product.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ctl.stk.product.entity.Product;
import com.ctl.stk.product.repository.ProductRepository;

/**
 * Product service implementation
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Service("productService")
public class ProductServiceImpl implements ProductService {

	/**
	 * Repository of product
	 */
	@Autowired
	ProductRepository productRepository;
	
	/**
	 * Get product by id
	 * @param product id
	 * @return product object
	 */
	@Override
	public Product getProductById(Long id) {
		return productRepository.findById(id).get();
	}

	/**
	 * Get all products
	 * @return list of products
	 */
	@Override
	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}
	
	/**
	 * Add product
	 * @param product as object
	 */
	@Override
	public Product addProduct(Product product) {
		return productRepository.save(product);
	}
}

package com.ctl.stk.shoppingcart.item.service;

import java.util.List;

import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;

/**
 * Shopping cart item service interface
 * @author Diego Andre Poli
 */
public interface ShoppingCartItemService {
	
	/**
	 * Delete shooping cart by id
	 * @param id
	 */
	void deleteById(Long id);
	
	/**
	 * Get all shopping cart items
	 * @return all shooping cart items
	 */
	List<ShoppingCartItem> getAll();
	
	/**
	 * Get shopping cart by id
	 * @param id of cart
	 * @return cart object
	 */
	ShoppingCartItem getItemtById(Long id);
	
	/**
	 * Change quantity
	 * @param item id
	 * @param item quantity
	 * @return shopping cart item
	 */
	ShoppingCartItem changeQuantity(Long id, int quantity);
	
	/**
	 * Add shopping cart item
	 * @param shopping cart item
	 * @return shopping cart item
	 */
	ShoppingCartItem addItem(ShoppingCartItem shoppingCartItem);
	
	/**
	 * Add item and calculate amount
	 * @param shopping cart item
	 * @return shopping cart item
	 */
	ShoppingCartItem addItemAndCalculateAmount(ShoppingCartItem shoppingCartItem);
}

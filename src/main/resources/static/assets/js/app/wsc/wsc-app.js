
/**
 * Wsc application controller
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
var wscController = {
		
	/**
	 * Product list
	 */
    products: ko.observableArray([new Product()]),
    
    /**
     * Cart amount
     */
    amount: ko.observable(),
    
    /**
     * Cart items
     */
    items: ko.observableArray([new CartItem()]),
    
    /**
     * Load all products
     */
    loadProducts: function () {
        var self = this;
        
        WscServices.executeGet(WscRoutes.getAllProduct()).then(function(data){
        	self.products(ko.mapping.fromJS(data)());
        });       
    },
    
    /**
     * Load shopping cart
     */
    loadShoppingCart: function () {
        var self = this;  
        
        WscServices.executeGet(WscRoutes.getShoppingCart()).then(function(data){
        	self.items(ko.mapping.fromJS(data.items)());
            self.amount(ko.mapping.fromJS(data.amount)());
        });
    },
    
    /**
     * Add product to cart
     */
    addProductToCart: function(data) {
    	var self = this;
    	var item = ko.observable(data);    	
    	
    	//can be used by knockout model or not :P
    	var request = { 'product': { 'id': item().id() }, 'quantity': 1 };
    	
    	WscServices.executePost(WscRoutes.getShoppingCartItems(), request).then(function(data){
    		wscController.loadShoppingCart();
    		WscNotify.addCartProduct();	
    	});
	},  
    
	/**
	 * Remove product to cart
	 */
    removeProductCart: function(data) {
    	var self = this;    	
    	var selectedItem = ko.observable(data);
    	var id = selectedItem().id();    
    	
    	WscServices.executeDelete(WscRoutes.getShoppingCartItem(id), {}).then(function(){
    		wscController.loadShoppingCart();
    		WscNotify.removeCartProduct();  			
    	});    
    },
	
    /**
     * Change quantity of product cart
     */
    changeQuantityCart: function(data) {
		var self = this;    	
    	var item = ko.observable(data);
    	
    	WscServices.executePut(WscRoutes.getShoppingCartItemQty(item().id(), item().quantity()), '').then(function(){
    		wscController.loadShoppingCart();
    		WscNotify.qtyCartProduct();    		
    	});
	}
};

/**
 * Onload items... :)
 */
window.onload= function() {
    ko.applyBindings(wscController);
    
    wscController.loadProducts();
    wscController.loadShoppingCart();
};


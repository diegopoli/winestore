package com.ctl.stk.shoppingcart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ctl.stk.shoppingcart.entity.ShoppingCart;

/**
 * Shopping cart repository
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Repository("cartRepository")
public interface ShoppingCartRepository extends JpaRepository<ShoppingCart, Long>{}

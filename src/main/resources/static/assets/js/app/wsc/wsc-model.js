/**
 * Knockout model for data manipulation
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */

/**
 * Product observable definition
 */
var Product = function(product) {
    var self = this;
    
    self.id = ko.observable();
    self.sku = ko.observable();
    self.name = ko.observable();    
    self.price = ko.observable();
    self.image = ko.observable();   
}

/**
 * Cart item observable definition
 */
var CartItem = function(item) {
	var self = this;
	
	self.id = ko.observable();
	self.quantity = ko.observable();
	self.product = ko.observable(new Product());
	self.amount = ko.observable();
}
   
/**
 * Cart observable definition
 */
var Cart = function(cart) {
    var self = this;
    
    self.id = ko.observable();
    self.amount = ko.observable();
    self.items = ko.observableArray([new CartItem()]);
}

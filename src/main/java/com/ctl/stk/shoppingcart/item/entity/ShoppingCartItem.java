package com.ctl.stk.shoppingcart.item.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.ctl.stk.product.entity.Product;

/**
 * Shopping cart item entity
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
@Entity
public class ShoppingCartItem {

	/**
	 * Indentification
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Shopping cart item product
	 */
	@OneToOne(cascade = CascadeType.PERSIST)
	private Product product;

	/**
	 * Shopping cart quantity
	 */
	private int quantity;

	/**
	 * Shopping cart amount
	 */
	@Column(precision=8, scale=2)
	private BigDecimal amount;
	
	/**
	 * Default constructor
	 */
	public ShoppingCartItem() {}
	
	/**
	 * Custom constructor on product and quantity item
	 * @param product
	 * @param quantity
	 */
	
	public ShoppingCartItem(Product product, int quantity) {
		setProduct(product);
		setQuantity(quantity);
		calculateAmount();
	}
	
	/**
	 * Get shopping cart id
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Set id
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Get product
	 * @return product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * Set product
	 * @param product
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * Get quantity
	 * @return quantity
	 */
	public int getQuantity() {
		return quantity;
	}
	
	/**
	 * Get quantity plus
	 * @return quantity sum one (1)
	 */
	public int getQuantityPlus() {
		return quantity + 1;
	}
	
	/**
	 * get quantity as bigdecimal
	 * @return quantity
	 */
	public BigDecimal getQuantityAsBigdecimal() {
		return new BigDecimal(getQuantity());
	}

	/**
	 * Set quantity
	 * @param quantity
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	/**
	 * Ser quantitu and calculate amount
	 * @param item quantity
	 */
	public void setQuantityAndCalculateAmount(int quantity) {
		setQuantity(quantity);
		calculateAmount();
	}
	
	/**
	 * Get amount
	 * @return amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Set amount
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Calculate amount
	 */
	public void calculateAmount() {
		this.amount = getProduct().getPrice().multiply(getQuantityAsBigdecimal());
	}
}

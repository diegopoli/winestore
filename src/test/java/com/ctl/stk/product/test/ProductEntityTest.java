package com.ctl.stk.product.test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.ctl.stk.product.entity.Product;

/**
 * Product entity test
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class ProductEntityTest {
	
	@Before
	public void init() {}
	
	/**
	 * Test default methods
	 * @throws Exception
	 */
	@Test
	public void testDefaultMethods() throws Exception {
		
		//create product
		Product product = new Product();
		product.setId(72423L);
		product.setName("Wine in label");
		product.setPrice(new BigDecimal(99.0));
		product.setImage("/assets/img.jpg");
		
		//test values
		assertEquals("72423", product.getId().toString());
		assertEquals("Wine in label", product.getName());
		assertEquals(new BigDecimal(99.0), product.getPrice());
		assertEquals("/assets/img.jpg", product.getImage());
		
	}
}

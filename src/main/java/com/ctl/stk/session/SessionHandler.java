package com.ctl.stk.session;

import javax.servlet.http.HttpServletRequest;

/**
 * Session handler
 * @author Diego Andre poli <diegoandrepoli@gmai.com>
 */
public class SessionHandler {
	
	/**
	 * Cart identification on customer session
	 */
	public static final String CART_ID = "cartId";

	/**
	 * Set cart identification
	 * @param customer request
	 * @param cart id 
	 */
	public static void setCartId(HttpServletRequest request, Long id) {
		request.getSession().setAttribute(CART_ID, id);
	}
	
	/**
	 * Get cart identification
	 * @param customer request
	 * @return cart id
	 */
	public static Long getCartId(HttpServletRequest request) {
		return (Long) request.getSession().getAttribute(CART_ID);
	}
	
	/**
	 * Set cart id is not exists
	 * @param customer request
	 * @param cart id
	 */
	public static void setCartIdIsNotExist(HttpServletRequest request, Long id) {
		if(getCartId(request) == null) {
			setCartId(request, id);			
		}
	}
	
	/**
	 * Is session cart
	 * @param customer request
	 * @return is cart id
	 */
	public static boolean isCart(HttpServletRequest request) {
		return (getCartId(request) != null);
	}
}

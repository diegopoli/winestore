package com.ctl.stk.shoppingcart.utils;

import java.math.BigDecimal;

import com.ctl.stk.shoppingcart.entity.ShoppingCart;
import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;

/**
 * Totalize amount cart value
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class TotalizeShoppingCartAmount {
	
	/**
	 * Amount cart value
	 */
	private BigDecimal amount;
	
	/**
	 * Default constructor
	 */
	public TotalizeShoppingCartAmount() {
		this.amount = new BigDecimal(0);
	}

	/**
	 * Get amount
	 * @return
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Add to amount 
	 * @param amount
	 */
	public void addAmount(BigDecimal amount) {
		this.amount = this.amount.add(amount);
	}

	/**
	 * Calculate amount
	 * @param cart
	 * @return amount
	 */
	public BigDecimal calculate(ShoppingCart cart) {
		for(ShoppingCartItem item : cart.getItems()) {
			addAmount(item.getAmount());
		}
		
		return getAmount();
	}
}

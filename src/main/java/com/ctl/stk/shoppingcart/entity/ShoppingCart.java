package com.ctl.stk.shoppingcart.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.ctl.stk.shoppingcart.item.entity.ShoppingCartItem;
import com.ctl.stk.shoppingcart.utils.TotalizeShoppingCartAmount;

/**
 * Cart entity
 * @author Diego Andre Poli
 */
@Entity
public class ShoppingCart {

	/**
	 * Cart identification
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * Cart amount
	 */
	@Column(precision=8, scale=2)
	private BigDecimal amount;
	
	/**
	 * List of sopping cart items
	 */
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	private List<ShoppingCartItem> items = new ArrayList<>();
	
	/**
	 * Get cart id
	 * @return id of cart
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Set cart id
	 * @param id of cart
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Get amount
	 * @return amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * Set amount
	 * @param amount
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * Get shopping cart items
	 * @return list of shooping cart items
	 */
	public List<ShoppingCartItem> getItems() {
		return items;
	}

	/**
	 * Set shopping cart items
	 * @param list of items
	 */
	public void setItems(List<ShoppingCartItem> items) {
		this.items = items;
		calculateAmount();
	}
	
	/**
	 * Add shopping cart item
	 * @param shopping cart item
	 */
	public void addItem(ShoppingCartItem item) {
		this.items.add(item);
		calculateAmount();
	}
	
	/**
	 * Calculate item amount
	 */
	public void calculateAmount() {
		setAmount((new TotalizeShoppingCartAmount()).calculate(this));	
	}
	
	/**
	 * Merge shopping cart item
	 * @param shopping cart item
	 * TODO: improve using a more performatic solution
	 */
	public void mergeItems(ShoppingCartItem item) {		
		for(ShoppingCartItem  it : getItems()) {
			if(it.getProduct().getId().equals(item.getProduct().getId())) {
				it.setQuantity(it.getQuantityPlus());
				return;
			}
		}
		
		addItem(item);
	}
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<t:template>
    <jsp:body>
        <div class="container">
   			<div class="row">
       			<div class="carousel slide hidden-xs" data-ride="carousel">
           			<div class="wsc-catalog">
               			<div class="item active">
                   			<div class="row" data-bind="foreach: wscController.products">
                       			<div class="col-sm-3 wsc-catalog-item">
                           			<div class="col-item">
                               			<div class="photo text-center">
                                   			<img data-bind="attr:{src: image}" class="img-responsive col-sm-10" alt="a" />
                               			</div>
                               			<div class="info">
                                   			<div class="row">
                                       			<div class="wsc-info col-md-12">
                                           			<div class="wsc-info-name" data-bind="text: name"></div>
                                           			<div class="wsc-info-price" data-bind="money: price"></div>
                                       			</div>
                                   			</div>
                                   			<div class="separator clear-left">
                                       			<p class="btn-add">                                           
				    								<a href="#" class="hidden-sm w-btn-cart" data-bind="click: wscController.addProductToCart">
														<i class="material-icons">check</i>
					    								Add to cart
				    								</a>
				    							</p>                                       				
                                   			</div>
                                   			<div class="clearfix"></div>
                               			</div>
                           			</div>
                       			</div>
                  			</div>
              			</div>
          			</div>
      			</div>
   			</div>
		</div>
    </jsp:body>
</t:template>
package com.ctl.stk.product.test;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.ctl.stk.product.controller.ProductController;
import com.ctl.stk.product.entity.Product;
import com.ctl.stk.product.service.ProductService;
import com.ctl.stk.shoppingcart.item.service.ShoppingCartItemService;
import com.ctl.stk.shoppingcart.service.ShoppingCartService;
import com.ctl.stk.utils.JsonStrinfy;

/**
 * Product test
 * @author Diego Andre Poli <diegoandrepoli@gmail.com>
 */
public class ProductTest {

	/**
	 * Mock
	 */
	private MockMvc mvc;

	/**
	 * Mock product service
	 */
	@Mock
	private ProductService productService;

	/**
	 * Mock cart service
	 */
	@Mock
	private ShoppingCartService cartService;

	/**
	 * Mock shopping cart item service
	 */
	@Mock
	private ShoppingCartItemService shoppingCartItemService;

	/**
	 * Mock product controller
	 */
	@InjectMocks
	private ProductController productController;

	/**
	 * Before run test
	 */
	@Before
	public void init() {
		//init mockito
		MockitoAnnotations.initMocks(this);
		
		//initialize controller
		mvc = MockMvcBuilders.standaloneSetup(productController).build();
	}

	/**
	 * Test add product api
	 * @throws Exception
	 */
	@Test
	public void testAddProduct() throws Exception {

		//create product
		Product product = new Product();
		product.setId(1L);
		product.setSku("34534");
		product.setName("Espumante Real De Aragon");
		product.setImage("/img.jpg");
		product.setPrice(new BigDecimal(9));
		
		//check is product exist
		when(productService.getProductById(product.getId())).thenReturn(product);

		//add product
		doReturn(product).when(productService).addProduct(product);

		//perform test
		mvc.perform(post("/products/add")
			.contentType(MediaType.APPLICATION_JSON)
			.content(JsonStrinfy.asJsonString(product)))
			.andExpect(status().isOk());
	}
	
	/**
	 * Test of get product
	 * @throws Exception
	 */
	@Test
	public void testGetProduct() throws Exception {
		
		//create product
		Product product = new Product();
		product.setId(1L);
		product.setSku("34534");
		product.setName("Espumante Real De Aragon");
		product.setImage("/img.jpg");
		product.setPrice(new BigDecimal(9));
				
		//mock service
		doReturn(product).when(productService).getProductById(product.getId());

		//perform test on get
		mvc.perform(get("/products/1")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(content().string(JsonStrinfy.asJsonString(product)));
	}
}

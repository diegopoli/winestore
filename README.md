# Overview

Wine store is a java spring boot and knockout js application a shooping cart and simple product catalog.

## Setup project

You must have installed java version 1.8 and maven, if yes so clone this repository.


```sh
git clone https://youuser@bitbucket.org/diegopoli/winestore.git
```

Navigate on the winestore folder and execute startup spring boot command.


```sh
mvn spring-boot:run
```

## Running tests

In project folder execute manven clean.


```sh
maven clean
```

After execute the install maven command.


```sh
maven install
```

## Release notes

* 0.0.1
	* Partial shopping cart feature.
  	* Product catalog.
 
* 0.0.2
  	* Fix on remove shoppign cart product.
  	* Refactor and performance shopping cart.

* 0.0.3
  	* Performance refactor and code clean.

## Future improvements

1. Setup a docker container.
2. Performing shopping cart models.
3. Add customer login.